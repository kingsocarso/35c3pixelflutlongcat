import socket
from PIL import Image
def rgb2hex(r, g, b):
    return '{:02x}{:02x}{:02x}'.format(r, g, b)
longcat = Image.open("longcat1.png")
#data = longcat.convert('RGB')
data = longcat.convert('RGBA')
#print(data[13, 12][0])
serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serversocket.connect(('151.217.40.82', 1234))
ylowbound = 330
xlowbound = 1000
while 1:
    for x in range(xlowbound, 1500):
        for y in range(ylowbound, 1080):
            #print("Sending: " + str(x) + ", " + str(y))
            x1 = x - xlowbound
            y1 = y - ylowbound
            #r, g, b = data.getpixel((x1, y1))
            r, g, b, a = data.getpixel((x1, y1))
            if a != 0:
                print(rgb2hex(r, g, b))
                serversocket.send('PX {} {} {}\n'.format(x,y,rgb2hex(r, g, b)).encode('ascii'))

